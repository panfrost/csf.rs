import json

p = json.load(open("registers.json"))

for o in p["control"]:
    r = int("0x" + o, 16)
    print(f'known_addresses.insert({hex(0x40003000 + r)}, \"{p["control"][o]}\".to_string());')
print('')

print('for (i, x) in ["COMPUTE", "FRAG", "TILER"].iter().enumerate() {')
print('     for r in 0..16 {')
for o in p["iterator"]:
    addr = hex(0x40031000 + int("0x" + o, 16))
    print(f'        known_addresses.insert({addr} + (i as u32)*0x1000 + (r as u32)*0x100, format!("{{}}_{p["iterator"][o]}\", x));')
print('     }')
print('}')
print('')

print('for i in 0..16 {')
for o in p["cshwif"]:
    addr = hex(0x40034000 + int("0x" + o, 16))
    print(f'    known_addresses.insert({addr} + (i as u32)*0x100, format!("CSHWIF{{}}_{p["cshwif"][o]}", i));')
print('}')

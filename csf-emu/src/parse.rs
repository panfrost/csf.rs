use nom::bytes::complete::{tag, take};
use nom::number::complete::{le_u16, le_u32, le_u8};
use nom::sequence::tuple;
use nom::IResult;

fn trim_null(bytes: &[u8]) -> &[u8] {
    match bytes.iter().position(|&x| x == 0) {
        Some(length) => &bytes[0..length],
        None => bytes,
    }
}

fn vec_to_string(bytes: Vec<u8>) -> String {
    String::from_utf8(trim_null(&bytes).to_vec()).unwrap()
}

#[derive(Debug)]
struct Header {
    version_major: u8,
    version_minor: u8,
    version_hash: u32,
    entry_end_offset: u32,
}

fn header(i: &[u8]) -> IResult<&[u8], Header> {
    let magic = tag([0x6E, 0x3A, 0xF1, 0xC3]);

    let (input, (_, version_minor, version_major, _, version_hash, _, entry_end_offset)) =
        tuple((magic, le_u8, le_u8, le_u16, le_u32, le_u32, le_u32))(i)?;

    Ok((
        input,
        Header {
            version_major,
            version_minor,
            version_hash,
            entry_end_offset,
        },
    ))
}

const INTERFACE: u8 = 0;
const CONFIGURATION: u8 = 1;
const TRACEBUFFER: u8 = 3;

#[derive(Debug)]
struct Entry {
    type_: u8,
    size: u8,
    flags: u16,
}

fn entry(i: &[u8]) -> IResult<&[u8], Entry> {
    let (input, (type_, size, flags)) = tuple((le_u8, le_u8, le_u16))(i)?;
    Ok((input, Entry { type_, size, flags }))
}

#[derive(Debug)]
pub struct Mapping {
    pub read: bool,
    pub write: bool,
    pub execute: bool,
    pub protected: bool,
    pub shared: bool,
    pub zero: bool,
    pub cached: bool,
    pub coherent: bool,
    pub virtual_start: u32,
    pub virtual_end: u32,
}

#[derive(Debug)]
pub struct Region {
    pub map: Mapping,
    pub data: Vec<u8>,
}

#[derive(Debug)]
struct Interface {
    map: Mapping,
    data_start: usize,
    data_end: usize,
}

const ENTRY_READ: u32 = 1 << 0;
const ENTRY_WRITE: u32 = 1 << 1;
const ENTRY_EXECUTE: u32 = 1 << 2;
const ENTRY_CACHED: u32 = 1 << 3;
const ENTRY_COHERENT: u32 = 1 << 4;
const ENTRY_PROTECTED: u32 = 1 << 5;
const ENTRY_SHARED: u32 = 1 << 30;
const ENTRY_ZERO: u32 = 1 << 31;

fn interface_memory_setup(i: &[u8]) -> IResult<&[u8], Interface> {
    let (i2, (flags, virtual_start, virtual_end, data_start, data_end)) =
        tuple((le_u32, le_u32, le_u32, le_u32, le_u32))(i)?;

    Ok((
        i2,
        Interface {
            map: Mapping {
                read: (flags & ENTRY_READ) != 0,
                write: (flags & ENTRY_WRITE) != 0,
                execute: (flags & ENTRY_EXECUTE) != 0,
                protected: (flags & ENTRY_PROTECTED) != 0,
                shared: (flags & ENTRY_SHARED) != 0,
                zero: (flags & ENTRY_ZERO) != 0,
                cached: (flags & ENTRY_CACHED) != 0,
                coherent: (flags & ENTRY_COHERENT) != 0,
                virtual_start,
                virtual_end,
            },
            data_start: data_start as usize,
            data_end: data_end as usize,
        },
    ))
}

#[derive(Debug)]
pub struct Config {
    pub address: u32,
    pub min: u32,
    pub max: u32,
    pub name: String,
}

fn config(i: &[u8], size: u8) -> IResult<&[u8], Config> {
    let (i2, (address, min, max)) = tuple((le_u32, le_u32, le_u32))(i)?;
    let (i3, name) = take(size - 16)(i2)?;

    Ok((
        i3,
        Config {
            name: vec_to_string(name.to_vec()),
            address,
            min,
            max,
        },
    ))
}

#[derive(Debug)]
pub struct Tracebuffer {
    pub type_: u32,
    pub size_address: u32,
    pub insert_address: u32,
    pub extract_address: u32,
    pub data_address: u32,
    pub trace_enable_address: u32,
    pub trace_enable_entry_count: u32,
    pub name: String,
}

fn tracebuffer(i: &[u8], size: u8) -> IResult<&[u8], Tracebuffer> {
    let (
        i2,
        (
            type_,
            size_address,
            insert_address,
            extract_address,
            data_address,
            trace_enable_address,
            trace_enable_entry_count,
        ),
    ) = tuple((le_u32, le_u32, le_u32, le_u32, le_u32, le_u32, le_u32))(i)?;
    let (i3, name) = take(size - 32)(i2)?;

    Ok((
        i3,
        Tracebuffer {
            type_,
            size_address,
            insert_address,
            extract_address,
            data_address,
            trace_enable_address,
            trace_enable_entry_count,
            name: vec_to_string(name.to_vec()),
        },
    ))
}

pub struct Firmware {
    pub regions: Vec<Region>,
    pub options: Vec<Config>,
    pub tracebuffers: Vec<Tracebuffer>,
}

pub fn parse(contents: &[u8]) -> IResult<&[u8], Firmware> {
    let (input1, header) = header(contents)?;

    let mut input = input1;
    let mut cursor = 0x14;
    let mut firmware = Firmware {
        regions: Vec::new(),
        options: Vec::new(),
        tracebuffers: Vec::new(),
    };

    while cursor < header.entry_end_offset {
        let (i2, entry) = entry(input)?;
        let (i3, data) = take(entry.size - 4)(i2)?;

        match entry.type_ {
            INTERFACE => {
                let (_, interface) = interface_memory_setup(data)?;
                let data = &contents[interface.data_start..interface.data_end];

                firmware.regions.push(Region {
                    map: interface.map,
                    data: data.to_vec(),
                })
            }
            CONFIGURATION => {
                let (_, cfg) = config(data, entry.size)?;

                firmware.options.push(cfg)
            }
            TRACEBUFFER => {
                let (_, tb) = tracebuffer(data, entry.size)?;

                firmware.tracebuffers.push(tb)
            }
            _ => println!("warning: Ignoring entry of type {}", entry.type_),
        }

        input = i3;
        cursor += entry.size as u32;
    }

    Ok((input, firmware))
}

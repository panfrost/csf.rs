use std::collections::HashMap;
use unicorn_engine::unicorn_const::{Arch, HookType, MemType, Mode, Permission};
use unicorn_engine::{RegisterARM, Unicorn};

mod parse;

use parse::{Firmware, Mapping, Tracebuffer};
use std::convert::TryInto;

const MMIO_BASE: u32 = 0x40_000000;
const SHARED_BASE: u32 = 0x04_000000;
const SHARED_SIZE: u32 = 0x94000;

// Private peripheral block registers
const PPB_BASE: u32 = 0xe000e000;
const PPB_SIZE: u32 = 0x1000;
const NVIC_ISER0: u32 = 0x100;
const PPB_SYST_RVR: u32 = 0xD14;
const PPB_SHCSR: u32 = 0xD24;
const PPB_CCSIDR: u32 = 0xD80;
const PPB_CSSELR: u32 = 0xD84;
const PPB_MPU_CTRL: u32 = 0xD94;
const PPB_MPU_RBAR: u32 = 0xD9C;
const PPB_MPU_RASR: u32 = 0xDA0;
const PPB_ICIALLU: u32 = 0xF50;
const PPB_DCISW: u32 = 0xF60;

// F50 and D80 so not M4/M23
// could be M7

/* Rust docs */
fn read_le_u32(input: &[u8]) -> u32 {
    let (int_bytes, _) = input.split_at(std::mem::size_of::<u32>());
    u32::from_le_bytes(int_bytes.try_into().unwrap())
}

struct Csf {
    output_buffer: Vec<u8>,
}

//fn find_tracebuffer<'a, 'b>(name: &'a str, firmware: &'b Firmware) -> Option<&'b Tracebuffer> {
//    firmware.tracebuffers.iter().find(|&x| x.name == name)
//}

const CODE_BASE: u64 = 0x1000000;

fn dump_firmware_info(firmware: &Firmware) {
    for opt in &firmware.options {
        println!("{:?}", opt)
    }

    for tb in &firmware.tracebuffers {
        println!("{:?}", tb)
    }

    println!("----");
    println!("Memory map");
    println!();

    for region in &firmware.regions {
        println!(
            "0x{:07x} - 0x{:07x}: {}{}{} {}{} {}{} {}",
            region.map.virtual_start,
            region.map.virtual_end,
            if region.map.read { "r" } else { "-" },
            if region.map.write { "w" } else { "-" },
            if region.map.execute { "x" } else { "-" },
            if region.map.protected { "P" } else { "-" },
            if region.map.shared { "S" } else { "-" },
            if region.map.cached { "C" } else { "-" },
            if region.map.coherent { "C" } else { "-" },
            if region.map.zero { "Z" } else { "-" },
        );
    }

    println!("----");
    println!();
}

fn permission_bool(perm: Permission, x: bool) -> Permission {
    if x {
        perm
    } else {
        Permission::NONE
    }
}

fn translate_permission(map: &Mapping) -> Permission {
    permission_bool(Permission::READ, map.read)
        | permission_bool(Permission::WRITE, map.write)
        | permission_bool(Permission::EXEC, map.execute)
}

fn map_regions<D>(emu: &mut Unicorn<D>, firmware: &Firmware) {
    for reg in &firmware.regions {
        let raw_size: usize = (reg.map.virtual_end - reg.map.virtual_start) as usize;
        let aligned_size = (raw_size + 4095) & !4095;
        let permission = translate_permission(&reg.map);

        if reg.map.shared {
            assert_eq!(reg.map.virtual_start, SHARED_BASE);
            assert_eq!(raw_size, SHARED_SIZE as usize);
        } else {
            emu.mem_map(reg.map.virtual_start as u64, aligned_size, permission)
                .expect("failed to map");
            emu.mem_write(reg.map.virtual_start as u64, &reg.data)
                .expect("failed to write");
        }
    }
}

fn read_vector<D>(emu: &mut Unicorn<D>, vector: u32) -> u32 {
    let addr = vector * 4;
    read_le_u32(&emu.mem_read_as_vec(addr as u64, 4).unwrap())
}

fn jump_to_vector<D>(emu: &mut Unicorn<D>, vector: u32, code_end: u64) {
    let address = read_vector(emu, vector);
    let res = emu.emu_start(address as u64, code_end, 0, 0);

    match res {
        Ok(()) => (),
        Err(err) => println!("Error emulating: {:?}", err),
    };
}

pub fn add_known_mmio(known_addresses: &mut HashMap<u32, String>) {
    known_addresses.insert(0x40003020, "GPU_RAWSTAT".to_string());
    known_addresses.insert(0x40003028, "GPU_IRQ_MASK".to_string());
    known_addresses.insert(0x4000302c, "GPU_IRQ_STATUS".to_string());
    known_addresses.insert(0x40003034, "GPU_STATUS".to_string());
    known_addresses.insert(0x4000303c, "GPU_FAULTSTATUS".to_string());
    known_addresses.insert(0x40003040, "GPU_FAULTADDRESS_LO".to_string());
    known_addresses.insert(0x40003044, "GPU_FAULTADDRESS_HI".to_string());

    for r in 0..16 {
        for (i, x) in ["COMPUTE", "FRAG", "TILER"].iter().enumerate() {
            known_addresses.insert(
                0x40031000 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_CTRL", x),
            );
            known_addresses.insert(
                0x40031004 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_STATUS", x),
            );
            known_addresses.insert(
                0x40031008 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_CONFIG", x),
            );
            known_addresses.insert(
                0x40031010 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_ENDPOINT_ALLOW_LO", x),
            );
            known_addresses.insert(
                0x40031014 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_ENDPOINT_ALLOW_HI", x),
            );
            known_addresses.insert(
                0x40031018 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_ENDPOINT_READY_LO", x),
            );
            known_addresses.insert(
                0x4003101c + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_ENDPOINT_READY_HI", x),
            );
            known_addresses.insert(
                0x40031020 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_QUEUE_COUNT", x),
            );
            known_addresses.insert(
                0x400310a0 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_BLOCKED_SB_ENTRY", x),
            );
            known_addresses.insert(
                0x400310a4 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_ENDPOINT_EVENT", x),
            );
            known_addresses.insert(
                0x400310c0 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_SHADER_DOORBELL_RAW", x),
            );
            known_addresses.insert(
                0x400310c4 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_SHADER_DOORBELL_MASK", x),
            );
            known_addresses.insert(
                0x400310cc + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_SHADER_DOORBELL_STATUS", x),
            );
            known_addresses.insert(
                0x400310d0 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_IRQ_RAW", x),
            );
            known_addresses.insert(
                0x400310d4 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_IRQ_MASK", x),
            );
            known_addresses.insert(
                0x400310dc + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_IRQ_STATUS", x),
            );
            known_addresses.insert(
                0x400310e0 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_FAULT_STATUS", x),
            );
            known_addresses.insert(
                0x400310e4 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_FAULT_SCOREBOARD", x),
            );
            known_addresses.insert(
                0x400310e8 + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_FAULT_ADDRESS_LO", x),
            );
            known_addresses.insert(
                0x400310ec + (r as u32) * 0x100 + (i as u32) * 0x1000,
                format!("{}_ITER_FAULT_ADDRESS_HI", x),
            );
        }
    }

    for i in 0..16 {
        known_addresses.insert(
            0x40034000 + (i as u32) * 0x100,
            format!("CSHWIF{}_CMD_PTR_LO", i),
        );
        known_addresses.insert(
            0x40034004 + (i as u32) * 0x100,
            format!("CSHWIF{}_CMD_PTR_HI", i),
        );
        known_addresses.insert(
            0x40034008 + (i as u32) * 0x100,
            format!("CSHWIF{}_CMD_PTR_END_LO", i),
        );
        known_addresses.insert(
            0x4003400c + (i as u32) * 0x100,
            format!("CSHWIF{}_CMD_PTR_END_HI", i),
        );
        known_addresses.insert(0x40034020 + (i as u32) * 0x100, format!("CSHWIF{}_CTRL", i));
        known_addresses.insert(
            0x40034024 + (i as u32) * 0x100,
            format!("CSHWIF{}_STATUS", i),
        );
        known_addresses.insert(
            0x40034028 + (i as u32) * 0x100,
            format!("CSHWIF{}_ITER_COMPUTE", i),
        );
        known_addresses.insert(
            0x4003402c + (i as u32) * 0x100,
            format!("CSHWIF{}_ITER_FRAGMENT", i),
        );
        known_addresses.insert(
            0x40034030 + (i as u32) * 0x100,
            format!("CSHWIF{}_ITER_TILER", i),
        );
        known_addresses.insert(
            0x40034034 + (i as u32) * 0x100,
            format!("CSHWIF{}_JASID", i),
        );
        known_addresses.insert(
            0x40034040 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_0_LO", i),
        );
        known_addresses.insert(
            0x40034044 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_0_HI", i),
        );
        known_addresses.insert(
            0x40034048 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_1_LO", i),
        );
        known_addresses.insert(
            0x4003404c + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_1_HI", i),
        );
        known_addresses.insert(
            0x40034050 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_2_LO", i),
        );
        known_addresses.insert(
            0x40034054 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_2_HI", i),
        );
        known_addresses.insert(
            0x40034058 + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_3_LO", i),
        );
        known_addresses.insert(
            0x4003405c + (i as u32) * 0x100,
            format!("CSHWIF{}_TRAP_CFG_3_HI", i),
        );
        known_addresses.insert(
            0x40034060 + (i as u32) * 0x100,
            format!("CSHWIF{}_EMULATION_INSTR_LO", i),
        );
        known_addresses.insert(
            0x40034064 + (i as u32) * 0x100,
            format!("CSHWIF{}_EMULATION_INSTR_LO", i),
        );
        known_addresses.insert(
            0x40034074 + (i as u32) * 0x100,
            format!("CSHWIF{}_WAIT_STATUS", i),
        );
        known_addresses.insert(
            0x40034078 + (i as u32) * 0x100,
            format!("CSHWIF{}_SB_SET_SEL", i),
        );
        known_addresses.insert(
            0x4003407c + (i as u32) * 0x100,
            format!("CSHWIF{}_SB_SEL", i),
        );
        known_addresses.insert(
            0x40034098 + (i as u32) * 0x100,
            format!("CSHWIF{}_EVENT_RAW", i),
        );
        known_addresses.insert(
            0x400340a0 + (i as u32) * 0x100,
            format!("CSHWIF{}_EVENT_IRQ_ENABLE", i),
        );
        known_addresses.insert(
            0x400340a4 + (i as u32) * 0x100,
            format!("CSHWIF{}_EVENT_IRQ_STATUS", i),
        );
        known_addresses.insert(
            0x400340a8 + (i as u32) * 0x100,
            format!("CSHWIF{}_EVENT_HALT_ENABLE", i),
        );
        known_addresses.insert(
            0x400340ac + (i as u32) * 0x100,
            format!("CSHWIF{}_EVENT_HALT_STATUS", i),
        );
        known_addresses.insert(
            0x400340b0 + (i as u32) * 0x100,
            format!("CSHWIF{}_FAULT_STATUS", i),
        );
        known_addresses.insert(
            0x400340b8 + (i as u32) * 0x100,
            format!("CSHWIF{}_FAULT_ADDRESS_LO", i),
        );
        known_addresses.insert(
            0x400340bc + (i as u32) * 0x100,
            format!("CSHWIF{}_FAULT_ADDRESS_HI", i),
        );
    }
}

fn main() {
    let contents = std::fs::read("mali_csffw.bin").unwrap();

    let firmware = match parse::parse(&contents) {
        Ok((_, x)) => x,
        _ => panic!(""),
    };

    dump_firmware_info(&firmware);

    let mut known_addresses = HashMap::new();

    for tb in &firmware.tracebuffers {
        [
            (tb.size_address, "size"),
            (tb.insert_address, "insert"),
            (tb.extract_address, "extract pointer"),
            (tb.data_address, "data buffer"),
            (tb.trace_enable_address, "enable"),
        ]
        .map(|(addr, label)| {
            known_addresses.insert(addr, format!("tracebuffer {} {}", tb.name, label))
        });
    }

    [
        (NVIC_ISER0, "NVIC_ISER0"),
        (PPB_SYST_RVR, "SYST_RVR"),
        (PPB_SHCSR, "SHCSR"),
        (PPB_CCSIDR, "CCSIDR"),
        (PPB_CSSELR, "CSSELR"),
        (PPB_MPU_CTRL, "MPU_CTRL"),
        (PPB_MPU_RBAR, "MPU_RBAR"),
        (PPB_MPU_RASR, "MPU_RASR"),
        (PPB_ICIALLU, "ICIALLU"),
        (PPB_DCISW, "DCISW"),
    ]
    .map(|(offset, label)| known_addresses.insert(PPB_BASE + offset, format!("PPB_{}", label)));

    add_known_mmio(&mut known_addresses);

    known_addresses.insert(0x40030004, "NR_OF_CSHWIF_AND_ITERATORS".to_string());

    for cshwif in 0..32 {
        known_addresses.insert(
            0x40030010 + cshwif * 4,
            format!("NR_OF_CSHWIF{}_REGS", cshwif),
        );

        for r in 0..256 {
            known_addresses.insert(
                0x40038000 + cshwif * 0x400 + r * 4,
                format!("CSHWIF{}_R{}", cshwif, r),
            );
        }
    }

    known_addresses.insert(0x4000000, "GLB_VERSION".to_string());
    known_addresses.insert(0x4000004, "GLB_FEATURES".to_string());
    known_addresses.insert(0x4000008, "GLB_INPUT_VA".to_string());
    known_addresses.insert(0x400000C, "GLB_OUTPUT_VA".to_string());
    known_addresses.insert(0x4000010, "GLB_GROUP_NUM".to_string());
    known_addresses.insert(0x4000014, "GLB_GROUP_STRIDE".to_string());
    known_addresses.insert(0x4000018, "GLB_PRFCNT_SIZE".to_string());

    let code_end = CODE_BASE + 65536;

    let mut emu = Unicorn::<Csf>::new_with_data(
        Arch::ARM,
        Mode::MCLASS,
        Csf {
            output_buffer: Vec::new(),
        },
    )
    .expect("failed to initialize Unicorn instance");
    map_regions(&mut emu, &firmware);

    //let tb = find_tracebuffer("fwlog", &firmware).unwrap();
    for tb in firmware.tracebuffers {
        emu.mem_write(tb.size_address as u64, &[0x0, 0x10, 0x0, 0x0])
            .unwrap();
        emu.mem_write(tb.data_address as u64, &[0x00, 0xbe, 0xad, 0xff])
            .unwrap(); // Trap accesses
        emu.mem_write(tb.insert_address as u64, &[0x40, 0xbe, 0x07, 0x4])
            .unwrap();
        emu.mem_write(tb.extract_address as u64, &[0x80, 0xbe, 0x07, 0x4])
            .unwrap();
        emu.mem_write(tb.trace_enable_address as u64, &[0xff, 0xff, 0xff, 0xff])
            .unwrap();
    }

    let cb_interrupt = move |uc: &mut Unicorn<'_, Csf>, intno: u32| {
        let r0 = uc.reg_read(RegisterARM::R0).expect("failed read");
        let r1 = uc.reg_read(RegisterARM::R1).expect("failed read");

        // We only implement the putchar interface
        assert_eq!(intno, 0x7);
        assert_eq!(r0, 0x3);

        // Dereference to get the character
        let c = uc.mem_read_as_vec(r1, 1).unwrap()[0];

        if c == b'\n' {
            let output_buffer = &mut uc.get_data_mut().output_buffer;
            println!("DBG: {}", String::from_utf8(output_buffer.clone()).unwrap());
            output_buffer.clear();
        } else {
            uc.get_data_mut().output_buffer.push(c);
        }

        // Advance the program counter past the breakpoint
        let pc = uc.reg_read(RegisterARM::PC).expect("failed read");
        uc.reg_write(RegisterARM::PC, (pc + 2) | 1)
            .expect("failed update of PC");
    };

    let label_address = |addr: u32| match known_addresses.get(&addr) {
        Some(x) => format!("{} ({:#04x})", x, addr),
        None => format!("unknown ({:#04x})", addr),
    };

    let read_callback = move |uc: &mut Unicorn<'_, Csf>, offset: u64, size: usize| {
        let pc = uc.reg_read(RegisterARM::PC).expect("failed read");

        println!(
            "mmio read {}, size={}, pc={:#04x}",
            label_address(MMIO_BASE + offset as u32),
            size,
            pc
        );

        match offset {
            0x30004 => {
                let nr_of_cshwif = 1;
                let nr_of_fragment_iterators = 1;
                let nr_of_compute_iterators = 1;
                let nr_of_tiler_iterators = 1;

                // bits 0... nr of cshwif
                // bits 6... nr of compute iterators
                // bits 11... nr of fragment iterators
                // bits 16... nr of tiler iterators
                nr_of_cshwif
                    | (nr_of_compute_iterators << 6)
                    | (nr_of_fragment_iterators << 11)
                    | (nr_of_tiler_iterators << 16)
            }
            0x30010 | 0x30014 | 0x30018 | 0x3001c => {
                // lower half: number of registers in CSHWIF#i register file
                // upper half: unknown
                8
            }
            _ => 0,
        }
    };

    let write_callback = move |uc: &mut Unicorn<'_, Csf>, offset, size, value| {
        let pc = uc.reg_read(RegisterARM::PC).expect("failed read");

        println!(
            "mmio write {}, size={}, pc={:#04x} <-- {:#04x}",
            label_address(MMIO_BASE + offset as u32),
            size,
            pc,
            value
        )
    };

    let shared_write_callback =
        move |uc: &mut Unicorn<'_, Csf>, offset: u64, size: usize, value: u64| {
            let pc = uc.reg_read(RegisterARM::PC).expect("failed read");

            println!(
                "shared write {}, size={}, pc={:#04x} <-- {:#04x}",
                label_address(SHARED_BASE + offset as u32),
                size,
                pc,
                value
            );

            if offset == 0x0000000 {
                // GLB_VERSION
                let patch = value & 0xffff;
                let minor = (value >> 16) & 0xff;
                let major = (value >> 24) & 0xff;

                println!("** CSF version {}.{}.{}", major, minor, patch)
            }
        };

    let shared_read_callback = move |uc: &mut Unicorn<'_, Csf>, offset: u64, size: usize| {
        let pc = uc.reg_read(RegisterARM::PC).expect("failed read");

        println!(
            "shared read {}, size={}, pc={:#04x}",
            label_address(SHARED_BASE + offset as u32),
            size,
            pc
        );

        0
    };

    let ppb_read_callback = move |_: &mut Unicorn<'_, Csf>, offset: u64, _size| {
        match offset as u32 {
            PPB_CCSIDR => 0xF003E019, // 4KB data cache, associativity=3
            PPB_SYST_RVR => 0x10000,
            //_ => panic!("don't know what to return "),
            _ => 0,
        }
    };

    let ppb_write_callback = move |_: &mut Unicorn<'_, Csf>, offset, size, value| {
        println!(
            "PPB write {}, size={} <-- {:#04x}",
            label_address(PPB_BASE + offset as u32),
            size,
            value
        )
    };

    emu.add_intr_hook(cb_interrupt)
        .expect("failed to add interrupt hook");

    // Presumably internal hardware registers
    emu.mmio_map(
        MMIO_BASE as u64,
        0x40000,
        Some(read_callback),
        Some(write_callback),
    )
    .expect("failed to map MMIO");

    // Private peripheral bus
    emu.mmio_map(
        PPB_BASE as u64,
        PPB_SIZE as usize,
        Some(ppb_read_callback),
        Some(ppb_write_callback),
    )
    .expect("failed to map MMIO");

    // Coherent shared memory handled like MMIO
    emu.mmio_map(
        SHARED_BASE as u64,
        SHARED_SIZE as usize,
        Some(shared_read_callback),
        Some(shared_write_callback),
    )
    .expect("failed to map shared");

    // Vector table is at 0x0 -- SP is at 0x0 and reset is at 0x4
    // https://developer.arm.com/documentation/dui0552/a/the-cortex-m3-processor/exception-model/vector-table
    let initial_sp = read_vector(&mut emu, 0);
    emu.reg_write(RegisterARM::SP, initial_sp as u64)
        .expect("failed write SP");

    jump_to_vector(&mut emu, 1, code_end); // RESET

    /* Trigger fault to dump registers */
    jump_to_vector(&mut emu, 2, code_end);
}
